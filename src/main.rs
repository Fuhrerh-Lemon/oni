use structopt::StructOpt;

mod profile;
mod rule;

use crate::profile::Profile;
use crate::rule::Rule;

#[derive(Debug, StructOpt)]
#[structopt(name = env!("CARGO_PKG_NAME"))]
#[structopt(version = option_env!("APP_VERSION").unwrap_or(env!("CARGO_PKG_VERSION")))]
#[structopt(author = env!("CARGO_PKG_AUTHORS"))]
#[structopt(about = env!("CARGO_PKG_DESCRIPTION"))]